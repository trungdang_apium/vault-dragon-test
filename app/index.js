'use strict';
let appModule = require('./modules/myapp');

/**
 * App Loader
 * 
 * @param {any} c Config
 */
module.exports = function appLoader(c) {
    return new appModule(c);
};