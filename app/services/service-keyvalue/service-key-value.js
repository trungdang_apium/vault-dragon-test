'use strict';

const _ = require('lodash');
const errors = require('feathers-errors');

/**
 * Key Value Service 
 * 
 * @class ServiceKeyValue
 */
class ServiceKeyValue {
	setup(app, path) {
		this.app = app;
		this.db = app.get('db');
	}

	get(id, params) {
		let db = this.db;
		let timestamp = null;
		if (params.query.hasOwnProperty('timestamp')) {
			timestamp = parseInt(params.query.timestamp);
		}

		return db
			.Get(id, timestamp)
			.then(data => {
				if (data == null) {
					throw new errors.NotFound('Key is not found');
				}
				return data.value;
			});
	}

	create(data, params) {
		let db = this.db;
		let possibleKeys = _.keys(data);

		if (possibleKeys.length == 0) {
			throw new errors.BadRequest('Key should not be empty');
		}

		let model = {
			key: possibleKeys[0],
			value: data[possibleKeys[0]]
		};

		return db.Save(model);
	}
}

module.exports = ServiceKeyValue