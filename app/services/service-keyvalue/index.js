'use strict';
/**
 * Key-Store-Value service
 */

const serviceKeyValue = require('./service-key-value');

module.exports = function exportServiceKeyStoreValue() {
    const app = this

    // Initialize our service with any options it requires
    app.use('/object', new serviceKeyValue());

    // update before / after hook if any
}