'use strict';

module.exports = function exportServices() {
    const app = this;

    const serviceKeyvalue = require('./service-keyvalue');

    app
        .configure(serviceKeyvalue);
};