'use strict';
let config = {};
const compress = require('compression');
const cors = require('cors');
const feathers = require('feathers');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const bodyParser = require('body-parser');
const Database = require('./database');
const errorHandler = require('feathers-errors/handler')

function createServerInstance(config) {
    let app = feathers();

    Object.keys(config).forEach(name => {
        let value = config[name];
        app.set(name, value);
    })

    const whitelist = app.get('corsWhitelist')
    const corsOptions = {
        origin(origin, callback) {
            const originIsWhitelisted = whitelist.indexOf(origin) !== -1
            callback(null, originIsWhitelisted)
        }
    }

    const services = require('../services');

    app.use(compress())
        .options('*', cors(corsOptions))
        .use(cors(corsOptions))
        .use(bodyParser.json({ limit: config.bodyParserLimit }))
        .use(bodyParser.urlencoded({ extended: true, limit: config.bodyParserLimit }))
        .configure(hooks())
        .configure(rest())
        .configure(services)
        .use(errorHandler());
    return app;
}

class MyApp {
    constructor(c) {
        config = c;
    }

    /**
     * Start listen the server 
     */
    start() {
        let app = createServerInstance(config);

        // register database 
        let db = new Database(config);
        app.set('db', db);

        let port = app.get('port');
        let host = app.get('host');
        let server = app.listen(port)
        server.on('listening', () => {
            console.log(`Application started on ${host}:${port}`);
        });

        return app
    }
}

module.exports = MyApp;