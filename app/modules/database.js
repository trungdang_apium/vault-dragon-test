'use strict';

/**
 * DAL
 */

const _ = require('lodash');
const tableName = 'keysvalue';
const timestampField = 'createdOn';
let knex;

/**
 * Return current UTC timestamp
 */
function getTimeStampUTC() {
    return ~~(+new Date() / 1000);
}

function createTableIfNotExist(knex) {
    knex.raw(`create table if not exists "${tableName}" ("id" integer not null primary key autoincrement, "key" varchar(255), "value" blob, "${timestampField}" integer)`)
        .catch((err) => {
            console.log(err);
        });
}

class Database {
    constructor(config) {
        knex = require('knex')({
            client: 'sqlite3',
            connection: {
                filename: config.dbConnection
            }
        });

        createTableIfNotExist(knex);
        this.connection = knex;
    }

    Save(model) {
        let knex = this.connection;

        return knex(tableName)
            .returning('id')
            .insert({
                key: model.key,
                value: model.value,
                [timestampField]: getTimeStampUTC()
            }).then(id => {
                if (id.length == 0) {
                    throw new Error('Could not save data into database');
                }
                let Id = id[0];
                return Id;
            });
    }

    Get(key, timestamp) {
        let knex = this.connection;
        let sql = knex.select('*')
                .from(tableName)
                .orderBy(timestampField, 'desc')
                .orderBy('id', 'desc')
                .limit(1);
        let where = {
            'key': key
        };
        if (timestamp > 0) {
            where[timestampField] = timestamp;
        }
        sql = sql.where(where);

        return sql.then(data => {
            let ret = null;
            if (data.length > 0) {
                ret = data[0];
            }

            return ret;
        });
    }
}

module.exports = function exportDatabaseWrapper(config) {
    console.log('init database ....');
    return new Database(config);
};