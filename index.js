'use strict';

let config = require('./config');

let appLoader = require('./app');

let appInstance = appLoader(config);

appInstance.start();