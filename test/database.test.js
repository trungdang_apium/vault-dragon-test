'use strict';

/**
 * integration test for database
 */
const assert = require('assert');
let db;

describe('Database integration test', function () {
    before((done) => {
        db = app.get('db');
        done();
    });

    it('Database should be registered', () => {
        assert.notStrictEqual(db, undefined);
    });

    it('Should save data into table keysvalue successfully', () => {
        let model = {
            key: 'myKey',
            value: 'myValue'
        };
        return db
            .Save(model)
            .then(result => {
                assert.equal(result > 0, true, 'Should return id of the inserted data');
            });
    });

    it('Should get existing data successfully', () => {
        let key = 'myKey';
        let value = 'myValue';

        return db
            .Get(key)
            .then(data => {
                assert.notStrictEqual(data, null, 'Data should not be null');
                assert.equal(data.key, key, 'Data should be same key');
                assert.equal(data.value, value, 'Data should be same value');
            });
    });
});