'use strict';

/**
 * Integration test
 */
const assert = require('assert');
const request = require('request');
let serverUrl = '';
let firstRequestTs = 0;
let expectKey = 'myKey';
let firstValue = 'myValue';
let secondValue = 'myValue2';

describe('MyApp', function () {
    before((done) => {
        let port = app.get('port');
        let host = app.get('host');
        serverUrl = `http://${host}:${port}`;
        done();
    });

    describe('#Service KeyValue', function () {
        it('Should be registered as /object endpoint resource', () => {
            let service = app.service('/object');
            assert.notStrictEqual(service, null);
        });

        it('GET should be registered', () => {
            let service = app.service('/object');
            assert.notStrictEqual(service.get, undefined);
        });

        it('POST should be registered', () => {
            let service = app.service('/object');
            assert.notStrictEqual(service.create, undefined);
        });

        describe('#POST', () => {
            it('Should save key successfull', function (done) {
                firstRequestTs = ~~(+new Date() / 1000);

                request.post({
                    url: `${serverUrl}/object`,
                    json: true,
                    form: {
                        [expectKey]: firstValue
                    }
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 201);
                    done(err);
                });
            });

            it('Should add new version of existing key successfully', (done) => {
                setTimeout(function () {
                    request.post({
                        url: `${serverUrl}/object`,
                        json: true,
                        form: {
                            [expectKey]: secondValue
                        }
                    }, function handleCallback(err, res, body) {
                        assert.equal(res.statusCode, 201);
                        done(err);
                    });
                }, 200);
            });

            it('Should throw code 400 if there is no key posted', () => {
                request.post({
                    url: `${serverUrl}/object`,
                    json: true,
                    form: {}
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 400);
                    done(err);
                });
            });
        });

        describe('#GET', () => {
            it('Should throw Error 405 if no key specify', (done) => {
                request.get({
                    url: `${serverUrl}/object`,
                    json: true
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 405);
                    done(err);
                });
            });

            it('Should Get value of a key successfully', (done) => {
                request.get({
                    url: `${serverUrl}/object/${expectKey}`,
                    json: true
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 200);
                    done(err);
                });
            });

            it('Should Get value of a key by timestamp successfully', (done) => {
                request.get({
                    url: `${serverUrl}/object/${expectKey}?timestamp=${firstRequestTs}`,
                    json: true
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 200);
                    assert.equal(res.body, secondValue);
                    done(err);
                });
            });

            it('Should return Code 404 Not Found if key not exist', (done) => {
                request.get({
                    url: `${serverUrl}/object/hello key`,
                    json: true
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 404);
                    done(err);
                });
            });
        });

        describe('Special cases', () => {
            let data = 'I am a very long with speci!@##$%%^^&&*()\r\n value'.repeat(65537);
            let expectDate = new Date();

            it(`Should accept POST long & special value with length = ${data.length}`, (done) => {
                request.post({
                    url: `${serverUrl}/object`,
                    json: true,
                    form: {
                        [expectKey]: data
                    }
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 201);
                    done(err);
                });
            });

            it('Should get correct long & special value', (done) => {
                request.get({
                    url: `${serverUrl}/object/${expectKey}`,
                    json: true
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 200);
                    assert.equal(res.body, data, 'Received data should be same');
                    done(err);
                });
            });

            it(`Should accept null value`, (done) => {
                request.post({
                    url: `${serverUrl}/object`,
                    json: true,
                    form: {
                        [expectKey]: null
                    }
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 201);
                    done(err);
                });
            });

            it(`Should accept date/time value`, (done) => {
                request.post({
                    url: `${serverUrl}/object`,
                    json: true,
                    form: {
                        [expectKey]: expectDate
                    }
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 201);
                    done(err);
                });
            });

            it(`Should get correct date/time value`, (done) => {
                request.get({
                    url: `${serverUrl}/object/${expectKey}`,
                    json: true,
                    form: {
                        [expectKey]: expectDate
                    }
                }, function handleCallback(err, res, body) {
                    assert.equal(res.statusCode, 200);
                    assert.equal(res.body, expectDate.toISOString(), 'Date should be same');
                    done(err);
                });
            });
        });
    });
});