'use strict';

module.exports = {
    env: 'integration-test',
    'host': 'localhost',
    'port': 3030,
    'corsWhitelist': [],
    dbConnection: ':memory:',
    bodyParserLimit: '15mb'
};