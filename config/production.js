'use strict';

module.exports = {
    env: 'production',
    'host': 'localhost',
    'port': 3030,
    'corsWhitelist': [],
    dbConnection: 'mydb.sqlite',
    bodyParserLimit: '15mb',
};