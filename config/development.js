'use strict';

module.exports = {
    env: 'development',
    'host': 'localhost',
    'port': 3030,
    'corsWhitelist': [],
    dbConnection: 'mydb.sqlite',
    bodyParserLimit: '15mb'
};