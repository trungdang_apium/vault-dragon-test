'use strict';

/**
 * Return current environment
 */
function getEnv(){
    return process.env.NODE_ENV || 'development';
}
/**
 * Get system config
 */
function getConfig(){
    let  env = getEnv();
    return require(`./${env}`);
}

module.exports = getConfig(); 