'use strict';

/**
 * Test Helper
 */
// force in-memory database
process.env.NODE_ENV = 'integration-test';

let config = require('../config');

let appLoader = require('../app');

let appInstance = appLoader(config);

// Let's register it into global scope
global.app = appInstance.start();